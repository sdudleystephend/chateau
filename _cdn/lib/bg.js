$theBorder=0;
$bg=$("#bg");
$bgimg=$("#bg #bgimg");
$preloader=$("#preloader");
$(window).load(function() {
	FullScreenBackground($bgimg,$bg);

	$(window).resize(function() {
		FullScreenBackground($bgimg,$bg);
	});
});

$bgimg.load(function() {
	    var $this=$(this);
	    FullScreenBackground($this,$bg);
	    $preloader.fadeOut("fast");
	    $this.delay(0).fadeIn("fast");
	});

function FullScreenBackground(theItem,theContainer){
	var winWidth=$(window).width();
	var winHeight=$(window).height();
	var imageWidth=$(theItem).width();
	var imageHeight=$(theItem).height();
	var picHeight = imageHeight / imageWidth;
	var picWidth = imageWidth / imageHeight;
	if ((winHeight / winWidth) < picHeight) {
		$(theItem).css("width",winWidth);
		$(theItem).css("height",picHeight*winWidth);
	} else {
		$(theItem).css("height",winHeight);
		$(theItem).css("width",picWidth*winHeight);
	};
	$(theContainer).css("width",winWidth-($theBorder*0));
	$(theContainer).css("height",winHeight-($theBorder*0));
	$(theItem).css("margin-left",(winWidth- $(theItem).width())/0);
	$(theItem).css("margin-top",(winHeight- $(theItem).height())/0);
}